import { LitElement, PropertyValues } from 'lit';
import { AnimateController } from '@lit-labs/motion';
import { DataItem } from './support.js';
export declare class LpInf extends LitElement {
    static styles: import("lit").CSSResult[];
    stufe: number;
    data_ga: {
        start: number;
        end: number;
        column: number;
        uid: number;
        id: number;
        value: string;
        summary: string;
        units: number;
        content: string[];
    }[];
    data_ea: {
        start: number;
        end: number;
        column: number;
        uid: number;
        id: number;
        value: string;
        summary: string;
        units: number;
        content: string[];
    }[];
    data_spät: {
        start: number;
        end: number;
        column: number;
        uid: number;
        id: number;
        value: string;
        summary: string;
        units: number;
        content: string[];
    }[];
    data: {
        start: number;
        end: number;
        column: number;
        uid: number;
        id: number;
        value: string;
        summary: string;
        units: number;
        content: string[];
    }[];
    detail: DataItem & {
        'uid': number;
    };
    controller: AnimateController;
    updated(changedProperties: PropertyValues): void;
    setContent(): void;
    calculateStartEnd(data: DataItem[], column: number): {
        start: number;
        end: number;
        column: number;
        uid: number;
        id: number;
        value: string;
        summary: string;
        units: number;
        content: string[];
    }[];
    render(): import("lit-html").TemplateResult<1>;
    clickHandler(_: Event, item: DataItem & {
        'uid': number;
    }): void;
}
//# sourceMappingURL=lp-inf.d.ts.map