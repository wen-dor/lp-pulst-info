export const springy = [
    0, 0.0701, 0.2329, 0.4308, 0.6245, 0.7906, 0.9184, 1.0065, 1.059, 1.0833,
    1.0872, 1.0783, 1.0628, 1.0453, 1.0288, 1.015, 1.0048, 0.9979, 0.994, 0.9925,
    0.9925, 0.9935, 0.9949, 0.9964, 0.9978, 0.999, 0.9998,
];
export const onFrames = (animate) => {
    const { animatingProperties: props, frames } = animate;
    if (frames === undefined || props === undefined) {
        return frames;
    }
    return [
        frames[0],
        ...springy.map((v) => {
            const frame = {};
            const x = props.left
                ? `translateX(${props.left * (1 - v)}px)`
                : '';
            const y = props.top
                ? `translateY(${props.top * (1 - v)}px)`
                : '';
            const sx = props.width
                ? `scaleX(${props.width + (1 - props.width) * v})`
                : '';
            const sy = props.height
                ? `scaleY(${props.height + (1 - props.height) * v})`
                : '';
            frame.transform = `${x} ${y} ${sx} ${sy}`;
            return frame;
        }),
        frames[1],
    ];
};
export const info_ga = [
    {
        id: 1, value: 'Rekursion', summary: '', units: 8,
        content: ['Die Schüler*innen analysieren und implementieren rekursive Algorithmen zur Lösung von Problemen und Aufgaben wie der Berechnung des ggT, der Erzeugung selbstähnlicher Figuren und der Türme von Hanoi. Sie erläutern das Prinzip der Rekursion und vergleichen iterative und rekursive Algorithmen für geeignete Problemstellungen',
            'Die Schüler*innen erläutern die Idee der Tiefensuche in Graphen, formulieren den zugehörigen Algorithmus und wenden diesen an konkreten Beispielen an. Sie implementieren die Tiefensuche in Graphen und modifizieren den Algorithmus in geeigneter, vom Anwendungskontext abhängiger Weise, z. B. bei der Auswahl oder Bearbeitung aller erreichbaren Knoten mit bestimmten Eigenschaften']
    },
    {
        id: 2, value: 'Listen', summary: '', units: 21,
        content: ['Die Schüler*innen modellieren mithilfe einfach verketteter Listen lineare Datenstrukturen aus verschiedenen Situationen ihres Lebensumfeldes, z. B. eine Playlist. Sie nutzen dabei das Konzept der Trennung von Struktur und Daten sowie das Entwurfsmuster Kompositum',
            'Die Schüler*innen entwickeln basierend auf ihrem Modell der einfach verketteten Liste Algorithmen zum Einfügen bzw. Löschen von Elementen an beliebiger Stelle sowie zum Durchlaufen der Liste, um z. B. Elemente zu suchen oder zu verändern. Dabei nutzen sie das Prinzip der Rekursion',
            'Die Schüler*innen erläutern die Kommunikation zwischen Objekten anhand gegebener Sequenzdiagramme, insbesondere zwischen den Objekten der Listenstruktur.',
            'Die Schüler*innen implementieren einfach verkettete Listen und die zugehörigen Algorithmen mithilfe einer objektorientierten Programmiersprache',
            'Die Schüler*innen nutzen bei der Modellierung und Implementierung von alltagsnahen Anwendungssituationen die flexible Verwendbarkeit einfach verketteter Listen; dabei setzen sie insbesondere die Datenstrukturen Stapel (LIFO) und Warteschlange (FIFO) um']
    },
    { id: 3, value: 'Binärbäume', summary: '', units: 14,
        content: [
            "Die SchülerInnen lernen, geordnete Binärbäume zu verschiedenen Problemstellungen zu modellieren. Sie nutzen dabei das Konzept der Trennung von Struktur und Daten und das Entwurfsmuster Kompositum.",
            "Es wird gelehrt, rekursive Algorithmen zur Verwaltung der Daten zu entwickeln, die in einem Binärbaum gespeichert sind. Dies beinhaltet vor allem die Traversierung eines Binärbaums sowie das Einfügen und Suchen in einem geordneten Binärbaum.",
            "Die SchülerInnen sollen lernen, geordnete Binärbäume mit Hilfe einer objektorientierten Programmiersprache zu implementieren.",
            "Das Verstehen und Bewerten von Struktur und Effizienz geordneter Binärbäume und einfach verketteter Listen im Kontext von Suchanfragen ist ein weiterer Teil des Lehrplans.",
            "Durch die Anwendung einer bereits implementierten Version eines geordneten Binärbaums auf praktische Anwendungssituationen, wird den SchülerInnen auch die Anwendung der Theorie vermittelt.",
            "Auch spezifische Inhalte und Kompetenzen wie die Definition und Eigenschaften von Bäumen und Binärbäumen, Traversierungsstrategien und Entwurfsmuster sind Teil des Lehrplans."
        ] },
    { id: 4, value: 'Nebenläufige Prozesse', summary: '', units: 9,
        content: [
            "Die Schüler*innen analysieren parallele Abläufe an Alltagsbeispielen, insbesondere bei der Nutzung gemeinsamer Ressourcen, und diskutieren die Notwendigkeit der Synchronisation.",
            "Sie lernen die Modellierung von gemeinsamer Ressourcennutzung in einem Betriebsmittelzuteilungsgraphen anhand einfacher Beispiele und analysieren diesen in Bezug auf mögliche Verklemmungssituationen.",
            "Sie erörtern die Coffman-Bedingungen und entwickeln daraus mögliche Maßnahmen zur Vermeidung von Verklemmungen in Anwendungssituationen.",
            "Die Schüler*innen lernen das Modellieren von nebenläufigen Prozessen oder Teilprozessen (Threads) mit Hilfe von Sequenzdiagrammen.",
            "Sie erläutern das Monitorkonzept als eine mögliche Strategie zur Synchronisation nebenläufiger Prozesse.",
            "Sie setzen einfache Beispiele mit nebenläufigen Prozessen oder Teilprozessen um und synchronisieren diese.",
            "Wichtige Inhalte und Kompetenzen sind: Nebenläufigkeit, Synchronisation, kritischer Abschnitt, wechselseitiger Ausschluss, Betriebsmittel, Betriebsmittelzuteilungsgraph, Verklemmung (Deadlock), Coffman-Bedingungen, ununterbrechbare Ressource, Monitorkonzept, leichtgewichtiger Prozess (Thread), Sequenzdiagramm, Erzeuger-Verbraucher-Problem und Philosophenproblem."
        ] },
    { id: 5, value: 'Informationssicherheit', summary: '', units: 6,
        content: [
            "Die Schüler*innen analysieren exemplarisch ein Informatiksystem hinsichtlich seiner Umsetzung wichtiger Schutzziele der Informationssicherheit und bewerten, ob diese Ziele erreicht wurden.",
            "Sie untersuchen verschiedene Arten der Gefährdung eines Informatiksystems und analysieren mögliche Angriffsszenarien.",
            "Sie erläutern verschiedene Maßnahmen zur Gewährleistung der Informationssicherheit und werden sich sowohl technischer als auch wirtschaftlicher Grenzen bewusst.",
            "Die Schüler*innen diskutieren verschiedene Perspektiven zu einer Fragestellung der Informationssicherheit, beispielsweise die Offenlegung oder Nichtoffenlegung von Schwachstellen, und berücksichtigen dabei individuelle und gesellschaftliche Auswirkungen.",
            "Die wichtigen Schutzziele der Informationssicherheit sind Vertraulichkeit, Integrität, Verfügbarkeit und Authentizität.",
            "Die Gefährdungskategorien umfassen höhere Gewalt, organisatorische Mängel, menschliche Fehlhandlungen, technisches Versagen und vorsätzliche Handlungen wie Schadprogramme und DDoS-Angriffe.",
            "Sicherheitsmaßnahmen können organisatorisch (z.B. durch Richtlinien und Passwortregeln), technisch (z.B. durch Antivirensoftware, Firewalls, Verschlüsselung und Sandboxing) oder physisch (z.B. durch Zugangskontrolle) sein."
        ] },
    { id: 6, value: 'Projekt: Softwareentwicklung', summary: '', units: 26,
        content: [
            "Die Schüler*innen lernen, Klassen aus Programmbibliotheken zu nutzen und die dazugehörige Dokumentation zu verwenden.",
            "Einfache grafische Benutzeroberflächen werden von den Schülern implementiert.",
            "Die Schüler*innen können die Grundidee des Architekturmusters Model-View-Controller erläutern.",
            "Sie überprüfen die Funktionalität von Softwareprodukten mithilfe verschiedener Testverfahren und verbessern den Quelltext durch Refaktorierung.",
            "Die Schüler*innen arbeiten eigenverantwortlich und im Team an einem Softwareprojekt, wobei sie dabei ein Vorgehensmodell der Softwareentwicklung und ein Versionsverwaltungssystem nutzen.",
            "Sie beschreiben und vergleichen Modelle für den Ablauf von Softwareprojekten, insbesondere das Wasserfallmodell und Modelle aus der agilen Softwareentwicklung.",
            "Inhaltlich umfasst dies unter anderem die Nutzung von Bibliotheksklassen, Grundlagen der Projektplanung, Testverfahren, und das Prinzip der Refaktorierung."
        ] }
];
export const info_spät = [
    { id: 1, value: 'Objektorientierte Modellierung und Programmierung', summary: '', units: 37, content: [
            "Schüler*innen analysieren und abstrahieren Objekte zu Klassen und stellen diese grafisch dar.",
            "Sie deklarieren Klassen, Attribute und Methoden in einer objektorientierten Programmiersprache.",
            "Schüler*innen implementieren Methoden basierend auf gegebenen oder selbst entwickelten Algorithmen.",
            "Beziehungen zwischen Objekten verschiedener Klassen werden anhand von praktischen Beispielen analysiert und modelliert.",
            "Die in Klassendiagrammen festgelegten Beziehungen werden durch Referenzen implementiert, um die Kommunikation zwischen Objekten zu ermöglichen.",
            "Sie modellieren Generalisierungshierarchien zu Strukturen aus ihrer Erfahrungswelt durch Klassendiagramme.",
            "Schüler*innen implementieren mithilfe einer objektorientierten Sprache Generalisierungshierarchien und wenden dabei das Konzept der Vererbung an.",
            "Das Konzept des Polymorphismus wird zur Implementierung verschiedener Verhaltensweisen erklärt.",
            "Lehrinhalte umfassen objektorientierte Konzepte wie Objekte, Klassen, Attribute und Methoden.",
            "Sie lernen verschiedene Arten von Variablen, Methoden und die Umsetzung von Beziehungen durch Referenzen kennen.",
            "Die Kommunikation zwischen Objekten und Datenkapselung durch kontrollierten Zugriff auf Attribute wird ebenso gelehrt wie die Verwendung von Sequenzdiagrammen.",
            "Sie lernen eindimensional indizierte Datenstrukturen (Arrays) und ihre Komponenten kennen.",
            "Konzepte von Generalisierung, Spezialisierung, Vererbung und Polymorphismus werden erläutert."
        ] },
    { id: 2, value: 'Rekursion', summary: '', units: 5, content: ['Die Schüler*innen analysieren und implementieren rekursive Algorithmen zur Lösung von Problemen und Aufgaben wie der Berechnung des ggT, der Erzeugung selbstähnlicher Figuren und der Türme von Hanoi. Sie erläutern das Prinzip der Rekursion und vergleichen iterative und rekursive Algorithmen für geeignete Problemstellungen'] },
    { id: 3, value: 'Liste', summary: '', units: 11, content: [
            "Die Schüler*innen lernen, wie sie lineare Datenstrukturen wie Playlisten aus ihrem Alltag mit einfach verketteten Listen modellieren können.",
            "Sie entwickeln Algorithmen für einfach verkettete Listen, um Elemente einzufügen, zu löschen oder die Liste zu durchlaufen und bestimmte Elemente zu suchen oder zu verändern. Dabei arbeiten sie mit dem Prinzip der Rekursion.",
            "Die einfach verketteten Listen und die dazu gehörigen Algorithmen implementieren sie in einer objektorientierten Programmiersprache.",
            "Im Unterricht werden folgende Inhalte vertieft: Der Aufbau einer einfach verketteten Liste, die rekursiven Methoden zum Einfügen, Entfernen und Suchen von Elementen und die Bestimmung der Listenlänge. Dabei wird auch die Trennung von Struktur und Daten bzw. Inhalt vermittelt."
        ] },
    { id: 4, value: 'Graphen', summary: '', units: 10, content: [
            "Die Schüler*innen sollten vernetzte Strukturen in Form von Graphen modellieren und klassifizieren können.",
            "Sie sollten die Fähigkeit haben, die Datenstruktur Graph mithilfe einer Adjazenzmatrix zu implementieren.",
            "Sie sollten die Tiefensuche verstehen, formulieren und an konkreten Beispielen anwenden können.",
            "Sie sollten in der Lage sein, den Tiefensuche-Algorithmus zu implementieren und in Abhängigkeit vom Anwendungskontext zu modifizieren.",
            "Die Schüler*innen sollten die Funktionsweise des Dijkstra-Algorithmus analysieren und erläutern sowie seine praktische Bedeutung verstehen können.",
            "In Bezug auf die Inhalte sollten sie Wissen zu den folgenden Punkten haben: Graph (Knoten, Kanten, Pfad, Erreichbarkeit von Knoten, Zyklus), Eigenschaften von Graphen (gerichtet, zusammenhängend, bewertet/gewichtet, zyklenfrei), Adjazenzmatrix, Tiefensuche, Dijkstra-Algorithmus."
        ] },
    { id: 5, value: 'Informationssicherheit', summary: '', units: 6, content: [
            "Die Schüler*innen analysieren exemplarisch ein Informatiksystem hinsichtlich seiner Umsetzung wichtiger Schutzziele der Informationssicherheit und bewerten, ob diese Ziele erreicht wurden.",
            "Sie untersuchen verschiedene Arten der Gefährdung eines Informatiksystems und analysieren mögliche Angriffsszenarien.",
            "Sie erläutern verschiedene Maßnahmen zur Gewährleistung der Informationssicherheit und werden sich sowohl technischer als auch wirtschaftlicher Grenzen bewusst.",
            "Die Schüler*innen diskutieren verschiedene Perspektiven zu einer Fragestellung der Informationssicherheit, beispielsweise die Offenlegung oder Nichtoffenlegung von Schwachstellen, und berücksichtigen dabei individuelle und gesellschaftliche Auswirkungen.",
            "Die wichtigen Schutzziele der Informationssicherheit sind Vertraulichkeit, Integrität, Verfügbarkeit und Authentizität.",
            "Die Gefährdungskategorien umfassen höhere Gewalt, organisatorische Mängel, menschliche Fehlhandlungen, technisches Versagen und vorsätzliche Handlungen wie Schadprogramme und DDoS-Angriffe.",
            "Sicherheitsmaßnahmen können organisatorisch (z.B. durch Richtlinien und Passwortregeln), technisch (z.B. durch Antivirensoftware, Firewalls, Verschlüsselung und Sandboxing) oder physisch (z.B. durch Zugangskontrolle) sein."
        ] },
    { id: 6, value: 'Projekt: Softwareentwicklung', summary: '', units: 15, content: [
            "Die Schüler*innen nutzen Klassen aus Programmbibliotheken, beispielsweise zur Implementierung einer grafischen Benutzeroberfläche.",
            "Sie lernen, die Phasen des Softwareprojektablaufs anhand des Wasserfallmodells zu beschreiben und diese mit einem Modell der agilen Softwareentwicklung zu vergleichen.",
            "Die Durchführung eines Softwareprojekts zu einer einfachen Aufgabe eigenverantwortlich und arbeitsteilig im Team ist ebenfalls Bestandteil des Lehrplans.",
            "Zu den Kompetenzen gehören die Arbeit mit Bibliotheksklassen, wie zum Beispiel Listen, Elemente einer grafischen Benutzeroberfläche, Taktgeber und Methoden zur dauerhaften Datenspeicherung.",
            "Die Schüler*innen lernen Grundlagen der Projektplanung, wie Zielsetzung, Arbeitsteilung, Schnittstellen und Meilensteine.",
            "In Bezug auf das Wasserfallmodell setzen sie sich mit den Schritten Analyse, Entwurf, Implementierung, Test, Bewertung und Abnahme, sowie Betrieb und Wartung auseinander.",
            "Die agile Softwareentwicklung lernen sie durch das agile Manifest und agile Methoden (wie User Stories, Tasks, Project Board, Sprints, Retrospektive) kennen und verstehen sie die iterative inkrementelle Softwareentwicklung.",
            "Ein weiterer Kompetenzbereich liegt im Bereich des Testens von Software. Die Schüler*innen erlernen manuelle Überprüfungen, automatisiertes Testen, Debuggen und das Testen einzelner Komponenten."
        ] }
];
export const info_ea = [
    { id: 1, value: 'Rekursion', summary: '', units: 8, content: ['Die Schüler*innen analysieren und implementieren rekursive Algorithmen zur Lösung von Problemen und Aufgaben wie der Berechnung des ggT, der Erzeugung selbstähnlicher Figuren und der Türme von Hanoi. Sie erläutern das Prinzip der Rekursion und vergleichen iterative und rekursive Algorithmen für geeignete Problemstellungen',
            'Die Schüler*innen erläutern die Idee der Tiefensuche in Graphen, formulieren den zugehörigen Algorithmus und wenden diesen an konkreten Beispielen an. Sie implementieren die Tiefensuche in Graphen und modifizieren den Algorithmus in geeigneter, vom Anwendungskontext abhängiger Weise, z. B. bei der Auswahl oder Bearbeitung aller erreichbaren Knoten mit bestimmten Eigenschaften'] },
    { id: 2, value: 'Listen', summary: '', units: 21, content: ['Die Schüler*innen modellieren mithilfe einfach verketteter Listen lineare Datenstrukturen aus verschiedenen Situationen ihres Lebensumfeldes, z. B. eine Playlist. Sie nutzen dabei das Konzept der Trennung von Struktur und Daten sowie das Entwurfsmuster Kompositum',
            'Die Schüler*innen entwickeln basierend auf ihrem Modell der einfach verketteten Liste Algorithmen zum Einfügen bzw. Löschen von Elementen an beliebiger Stelle sowie zum Durchlaufen der Liste, um z. B. Elemente zu suchen oder zu verändern. Dabei nutzen sie das Prinzip der Rekursion',
            'Die Schüler*innen erläutern die Kommunikation zwischen Objekten anhand gegebener Sequenzdiagramme, insbesondere zwischen den Objekten der Listenstruktur.',
            'Die Schüler*innen implementieren einfach verkettete Listen und die zugehörigen Algorithmen mithilfe einer objektorientierten Programmiersprache',
            'Die Schüler*innen nutzen bei der Modellierung und Implementierung von alltagsnahen Anwendungssituationen die flexible Verwendbarkeit einfach verketteter Listen; dabei setzen sie insbesondere die Datenstrukturen Stapel (LIFO) und Warteschlange (FIFO) um'] },
    { id: 3, value: 'Binärbäume', summary: '', units: 16, content: [
            "Die SchülerInnen lernen, geordnete Binärbäume zu verschiedenen Problemstellungen zu modellieren. Sie nutzen dabei das Konzept der Trennung von Struktur und Daten und das Entwurfsmuster Kompositum.",
            "Es wird gelehrt, rekursive Algorithmen zur Verwaltung der Daten zu entwickeln, die in einem Binärbaum gespeichert sind. Dies beinhaltet vor allem die Traversierung eines Binärbaums sowie das Einfügen und Suchen in einem geordneten Binärbaum.",
            "Die SchülerInnen sollen lernen, geordnete Binärbäume mit Hilfe einer objektorientierten Programmiersprache zu implementieren.",
            "Das Verstehen und Bewerten von Struktur und Effizienz geordneter Binärbäume und einfach verketteter Listen im Kontext von Suchanfragen ist ein weiterer Teil des Lehrplans.",
            "Durch die Anwendung einer bereits implementierten Version eines geordneten Binärbaums auf praktische Anwendungssituationen, wird den SchülerInnen auch die Anwendung der Theorie vermittelt.",
            "Auch spezifische Inhalte und Kompetenzen wie die Definition und Eigenschaften von Bäumen und Binärbäumen, Traversierungsstrategien und Entwurfsmuster sind Teil des Lehrplans.",
            "Zusätzlich im erhöhten Anforderungsniveau: Die SchülerInnen erläutern an konkreten Beispielen, wie Bäume, die keine geordneten Binärbaume sind, zur Lösung praxisrelevanter Aufgaben verwendet werden, z. B. Huffman-Baum zur Umsetzung einer Textkompression, Quad-Tree zur Flächenindizierung oder Hash-Baum zur Sicherstellung der Integrität von Daten."
        ] },
    { id: 4, value: 'Funktionsweise eines Rechners', summary: '', units: 26, content: [
            "Die Schüler*innen lernen die grundlegenden Komponenten eines Computers und vergleichen diese mit der Von-Neumann-Architektur, da diese als Grundlage moderner Computersysteme dient.",
            "Die Studierenden erweitern ihre Kenntnisse über die Darstellung von natürlichen Zahlen im Binärsystem, einschließlich der Darstellung ganzer Zahlen, die das Zweierkomplement verwenden. Sie untersuchen auch die Grenzen des Zahlbereichs für Binärzahlen mit einer festen Anzahl von Stellen und führen Berechnungen mit Überlauf durch.",
            "Die Schüler*innen lernen, wie mithilfe von Transistorschaltungen elementare logische Funktionen (wie AND, OR, NOT) auf Hardware-Ebene in Form von Logikgattern realisiert werden können.",
            "Die Schüler*innen lernen, wie n-stellige logische Funktionen dargestellt und in Schaltungen umgewandelt werden können, die auf Logikgattern basieren. Anhand von Beispielen (wie dem Halbaddierer und dem Volladdierer) simulieren die Schüler*innen diese Prozesse mit geeigneter Software, um ein Verständnis des Rechenablaufs auf Hardware-Ebene zu erlangen.",
            "Die Schüler*innen lernen, wie Probleme mit logischen Funktionen modelliert werden können und dass die Beschreibung im Rahmen einer disjunktiven Normalform in der Regel nicht zu der einfachsten Realisierung einer Schaltung führt.",
            "Die Schüler*innen befassen sich mit der Möglichkeit, jede logische Funktion nur durch die NAND-Funktion darzustellen, und untersuchen die daraus resultierenden Möglichkeiten für die technische Umsetzung von Hardwarekomponenten.",
            "Die Schüler*innen setzen sich mit dem Aufbau und der Funktionsweise einer Registermaschine auseinander, lernen die Abarbeitung eines Programms durch eine solche Maschine und beschreiben den zugrunde liegenden Algorithmus",
            "Die Schüler*innen wenden einfache Algorithmen in Assemblersprache an und testen Programme mithilfe einer Registermaschinensimulation."
        ] },
    { id: 5, value: 'Betriebssysteme, Prozesse, Nebenläufigkeit', summary: '', units: 23, content: [
            "SchülerInnen erklären den Zweck und die Aufgaben eines Betriebssystems und beschreiben dessen prinzipiellen Aufbau anhand eines Schalenmodells.",
            "Sie vergleichen Anforderungen an Betriebssysteme in verschiedenen Einsatzszenarien, wie zum Beispiel Einzel- und Mehrbenutzerbetrieb.",
            "Sie sind in der Lage, die Funktionen und Mechanismen von Betriebssystemen zu nutzen und zu beschreiben, die dem Schutz und der Sicherheit von elektronischen Geräten dienen. Sie erklären auch, welche Sicherheitsrisiken mit Betriebssystemen verbunden sein können.",
            "Sie können verschiedene Scheduling-Strategien beschreiben und bewerten, die von Betriebssystemen zur Prozessverwaltung verwendet werden.",
            "Sie analysieren parallele Abläufe an Alltagsbeispielen, und begründen die Notwendigkeit der Synchronisation.",
            "Sie modellieren die Nutzung gemeinsamer Ressourcen und analysieren sie hinsichtlich möglicher Verklemmungssituationen. Sie erklären die Coffman-Bedingungen und entwickeln daraus mögliche Maßnahmen zur Vermeidung von Verklemmungen.",
            "Sie können nebenläufige Prozesse oder Teilprozesse mithilfe von Sequenzdiagrammen modellieren.",
            "Sie erklären das Monitor- und das Semaphorkonzept als mögliche Strategien zur Synchronisation nebenläufiger Prozesse.",
            "Sie implementieren Beispiele mit nebenläufigen Prozessen und synchronisieren diese.",
            "Die Inhalte umfassen zentrale Aufgaben eines Betriebssystems, Betriebssystemkern, Zugriffsrechte, Prozesszustände, Schedulingstrategien, Nebenläufigkeit, Synchronisation, Betriebsmittel, Verklemmung und Monitorkonzept."
        ] },
    { id: 6, value: 'Informationssicherheit', summary: '', units: 6, content: [
            "Die Schüler*innen analysieren exemplarisch ein Informatiksystem hinsichtlich seiner Umsetzung wichtiger Schutzziele der Informationssicherheit und bewerten, ob diese Ziele erreicht wurden.",
            "Sie untersuchen verschiedene Arten der Gefährdung eines Informatiksystems und analysieren mögliche Angriffsszenarien.",
            "Sie erläutern verschiedene Maßnahmen zur Gewährleistung der Informationssicherheit und werden sich sowohl technischer als auch wirtschaftlicher Grenzen bewusst.",
            "Die Schüler*innen diskutieren verschiedene Perspektiven zu einer Fragestellung der Informationssicherheit, beispielsweise die Offenlegung oder Nichtoffenlegung von Schwachstellen, und berücksichtigen dabei individuelle und gesellschaftliche Auswirkungen.",
            "Die wichtigen Schutzziele der Informationssicherheit sind Vertraulichkeit, Integrität, Verfügbarkeit und Authentizität.",
            "Die Gefährdungskategorien umfassen höhere Gewalt, organisatorische Mängel, menschliche Fehlhandlungen, technisches Versagen und vorsätzliche Handlungen wie Schadprogramme und DDoS-Angriffe.",
            "Sicherheitsmaßnahmen können organisatorisch (z.B. durch Richtlinien und Passwortregeln), technisch (z.B. durch Antivirensoftware, Firewalls, Verschlüsselung und Sandboxing) oder physisch (z.B. durch Zugangskontrolle) sein."
        ] },
    { id: 7, value: 'Projekt: Softwareentwicklung', summary: '', units: 40, content: [
            "Die Schüler*innen beurteilen die Qualität und Gebrauchstauglichkeit eines Softwareprodukts unter Berücksichtigung spezifischer Kriterien.",
            "Sie entwerfen und implementieren einfache grafische Benutzeroberflächen, unter Berücksichtigung der Prüfkriterien der Softwareergonomie.",
            "Die Schüler*innen bewerten das Wasserfallmodell und ein agiles Vorgehensmodell in Bezug auf deren Beitrag zur Sicherstellung der Softwarequalität.",
            "Sie setzen Entwurfs- oder Architekturmuster sowie Programmbibliotheken ein, dokumentieren und refaktorisieren den Quellcode und führen verschiedene Tests zur Überprüfung der Funktionalität von Softwareprodukten durch.",
            "Die Schüler*innen führen ein Softwareprojekt durch, das sich auf eine praktische Aufgabenstellung bezieht. Dabei verwenden sie ein Vorgehensmodell der Softwareentwicklung, ein Versionsverwaltungssystem und wenden Maßnahmen des Softwarequalitätsmanagements an. Sie reflektieren ihren Arbeitsprozess und achten auf Aspekte des Urheberrechts und Datenschutzes.",
            "Ihre Kenntnisse umfassen Merkmale der Softwarequalität, Prüfkriterien der Softwareergonomie, Bibliotheksklassen, Entwurfs- oder Architekturmuster und Grundlagen der Projektplanung.",
            "Sie verstehen das Wasserfallmodell, die agile Softwareentwicklung und deren Methoden, sowie diverse Testmethoden.",
            "Refaktorierung zur Verbesserung von Lesbarkeit und Erweiterbarkeit, und Dokumentation des Projekts gemäß dem ausgewählten Vorgehensmodell, sind zentrale Elemente des Lehrplans."
        ] }
];
export const info_ga_13 = [
    {
        id: 1, value: 'Rekursion', summary: '', units: 8,
        content: ['Die Schüler*innen analysieren und implementieren rekursive Algorithmen zur Lösung von Problemen und Aufgaben wie der Berechnung des ggT, der Erzeugung selbstähnlicher Figuren und der Türme von Hanoi. Sie erläutern das Prinzip der Rekursion und vergleichen iterative und rekursive Algorithmen für geeignete Problemstellungen',
            'Die Schüler*innen erläutern die Idee der Tiefensuche in Graphen, formulieren den zugehörigen Algorithmus und wenden diesen an konkreten Beispielen an. Sie implementieren die Tiefensuche in Graphen und modifizieren den Algorithmus in geeigneter, vom Anwendungskontext abhängiger Weise, z. B. bei der Auswahl oder Bearbeitung aller erreichbaren Knoten mit bestimmten Eigenschaften']
    },
    {
        id: 2, value: 'Listen', summary: '', units: 21,
        content: ['Die Schüler*innen modellieren mithilfe einfach *verketteter Listen* lineare Datenstrukturen aus verschiedenen Situationen ihres Lebensumfeldes, z. B. eine Playlist. Sie nutzen dabei das Konzept der Trennung von Struktur und Daten sowie das Entwurfsmuster Kompositum',
            'Die Schüler*innen entwickeln basierend auf ihrem Modell der einfach verketteten Liste Algorithmen zum Einfügen bzw. Löschen von Elementen an beliebiger Stelle sowie zum Durchlaufen der Liste, um z. B. Elemente zu suchen oder zu verändern. Dabei nutzen sie das Prinzip der Rekursion',
            'Die Schüler*innen erläutern die Kommunikation zwischen Objekten anhand gegebener Sequenzdiagramme, insbesondere zwischen den Objekten der Listenstruktur.',
            'Die Schüler*innen implementieren einfach verkettete Listen und die zugehörigen Algorithmen mithilfe einer objektorientierten Programmiersprache',
            'Die Schüler*innen nutzen bei der Modellierung und Implementierung von alltagsnahen Anwendungssituationen die flexible Verwendbarkeit einfach verketteter Listen; dabei setzen sie insbesondere die Datenstrukturen Stapel (LIFO) und Warteschlange (FIFO) um']
    },
    { id: 3, value: 'Binärbäume', summary: '', units: 14,
        content: [] },
    { id: 4, value: 'Nebenläufige Prozesse', summary: '', units: 9,
        content: [] },
    { id: 5, value: 'Informationssicherheit', summary: '', units: 6,
        content: [] },
    { id: 6, value: 'Projekt: Softwareentwicklung', summary: '', units: 26,
        content: [] }
];
export const info_spät_13 = [
    { id: 1, value: 'Formale Sprachen und Automaten', summary: '', units: 16, content: [
            "Die Schüler*innen untersuchen formale Sprachen aus ihrem Alltag und finden Regeln für die Bildung der Wortsätze dieser Sprachen.",
            "Sie definieren Grammatiken zur Bildung formaler Sprachen und nutzen dabei insbesondere die Erweiterte Backus-Naur-Form und Syntaxdiagramme.",
            "Zum Unterscheiden regulärer Sprachen konzipieren sie endliche Automaten.",
            "Sie implementieren deterministische endliche Automaten zur automatisierten Überprüfung der Zugehörigkeit von Wörtern zu einer regulären Sprache.",
            "Anhand von Beispielen (wie geschachtelte Klammerausdrücke) wird erklärt, dass nicht alle Sprachen regulär sind und für ihre automatisierte Verarbeitung das Modell des endlichen Automaten möglicherweise nicht ausreichend ist.",
            "Die Inhalte enthalten auch Kompetenzen in Bezug auf formale Sprache als eine Menge von Wörtern aus einem Alphabet, inklusive Zeichen, Alphabet, Wort, Syntax, Semantik.",
            "Es werden Kenntnisse zur Grammatik geschaffen, einschließlich Terminal, Nichtterminal, Produktionsregel, Startsymbol.",
            "Sie lernen auch die Notation formaler Sprachen kennen, einschließlich Syntaxdiagramm und Erweiterter Backus-Naur-Form.",
            "Dazu gehört auch das Ableiten eines Wortes einer formalen Sprache als Folge von Regelanwendungen, sowie der Ableitungsbaum.",
            "Die Konzepte eines endlichen Automaten, sowohl deterministisch als auch nichtdeterministisch, werden studiert, einschließlich Zustandsmenge, Eingabealphabet, Zustandsübergang, Startzustand, Endzustand und Fehlerzustand, sowie die Definition einer regulären Sprache."
        ] },
    { id: 2, value: 'Funktionsweise eines Rechners', summary: '', units: 16, content: [
            "Die Schüler*innen sollen in der Lage sein, die Schlüsselkomponenten eines Computersystems zu beschreiben und diese mit der Von-Neumann-Architektur zu vergleichen. Sie sollten erkennen, dass die Von-Neumann-Architektur die Grundlage für moderne Computersysteme bildet.",
            "Die Schüler*innen sollen den Aufbau und die Funktionsweise einer Registermaschine erklären können.",
            "Die Schüler*innen sollen in der Lage sein, die Umsetzung eines Programms durch eine Registermaschine zu erläutern und den zugrundeliegenden Algorithmus zu beschreiben. Dies kann mit einer höheren Programmiersprache oder einem Struktogramm geschehen.",
            "Die Schüler*innen sollen in der Lage sein, einfache Algorithmen in Assemblersprache umzusetzen und diese Programme mit einer Registermaschine zu testen.",
            "Die Inhalte zu diesen Kompetenzen umfassen die Von-Neumann-Architektur als grundlegendes Modell für moderne Rechner, einschließlich Prozessor, Speicher, Eingabe- und Ausgabeeinheit und Bussystem.",
            "Die Registermaschine als Teil der Inhalte umfasst Akkumulator, Befehlsregister, Befehlszähler, Statusregister und Befehlszyklus.",
            "Die Schüler*innen sollen auch Kontrollstrukturen in der Assemblersprache kennen."
        ] },
    { id: 3, value: 'Grenzen der Berechenbarkeit', summary: '', units: 13, content: [
            "Schüler*innen bewerten den Laufzeitaufwand von Algorithmen durch Zeitmessungen und Zählverfahren.",
            "Sie begründen, warum ein hoher Laufzeitaufwand sicherheitsrelevant sein kann, beispielsweise bei der Anwendung des Brute-Force-Verfahrens zur Passwortsuche.",
            "Sie erläutern anhand des Halteproblems, dass es Probleme gibt, die nicht automatisiert lösbar sind.",
            "Sie lernen den Laufzeitaufwand von Algorithmen in verschiedenen Szenarien kennen: linear, quadratisch (als Beispiele für polynomiales Laufzeitverhalten), exponentiell und logarithmisch. Dabei behandeln sie auch die Begriffe Best Case, Average Case und Worst Case.",
            "Sie erwerben Kenntnisse über das Brute-Force-Verfahren.",
            "Sie beschäftigen sich mit dem Halteproblem und lernen, wie man einen Widerspruchsbeweis führt."
        ] },
    { id: 4, value: 'Künstliche Intelligenz', summary: '', units: 10, content: [
            "Die Schüler*innen sollen den grundsätzlichen Aufbau künstlicher neuronaler Netze erklären können und Forward Propagation zu gegebenen Eingabewerten ausführen.",
            "Sie sollen analysieren, wie Trainingsdaten und Hyperparameter, vor allem die Anzahl von Schichten und Neuronen, den Lernerfolg künstlicher neuronaler Netze beeinflussen.",
            "Sie sollen die Funktion des k-Means-Algorithmus als Beispiel unüberwachten Lernens beschreiben und anhand eines Beispiels implementieren.",
            "Zusätzlich sollen sie für verschiedene Eingabedaten analysieren, welche Ergebnisse der k-Means-Algorithmus in Abhängigkeit von k liefert.",
            "Sie sollen die Grundprinzipien überwachten Lernens (Supervised Learning), unüberwachten Lernens (Unsupervised Learning) und bestärkenden Lernens (Reinforcement Learning) erläutern.",
            "Sie sollen zu aktuellen Einsatzmöglichkeiten des maschinellen Lernens Stellung nehmen, Chancen und Risiken abwägen und diese im Hinblick auf individuelle und gesellschaftliche Verantwortung beurteilen.",
            "In Bezug auf die Inhalte sollen sie mit neuronalen Netzen und ihren Komponenten (Eingabe-, Ausgabeschicht, verdeckte Schicht, Gewichte), Forward Propagation und Fehlerrückführung (Backpropagation), Kostenfunktion umgehen können.",
            "Zudem sollen sie mit Clustering und dem k-Means-Algorithmus vertraut sein.",
            "Sie sollen die Unterschiede und Anwendungsmöglichkeiten von überwachtem Lernen (Supervised Learning), unüberwachtem Lernen (Unsupervised Learning) und verstärkendem Lernen (Reinforcement Learning) kennen.",
            "Schließlich sollen sie sich mit ethischen Fragen im Zusammenhang mit künstlicher Intelligenz auseinandersetzen."
        ] }
];
export const info_ea_13 = [
    { id: 1, value: 'Internet der Dinge', summary: '', units: 15, content: [
            "Die Schüler*innen sollen den Begriff 'Internet der Dinge' (IoT) verstehen und die verschiedenen Einsatzmöglichkeiten von IoT-Systemen beschreiben können, wie z.B. in einem Smart Home.",
            "Sie sollen in der Lage sein, Anwendungen für eine Physical-Computing-Plattform zu entwickeln, um Messdaten mit Sensoren zu erfassen und Aktoren zu steuern.",
            "Die Schüler*innen sollen gemeinsam eine Client-Server-Anwendung in einem lokalen Netzwerk unter Verwendung einer Physical-Computing-Plattform entwickeln können. Diese Anwendung sollte in der Lage sein, Daten zu erfassen, zu verarbeiten, darzustellen und persistenter zu speichern, z.B. mit einem Datenbankserver.",
            "Die Schüler*innen sollen auch in der Lage sein, IoT-Systeme zu analysieren, die über das lokale Netzwerk hinaus zugänglich sind, insbesondere im Hinblick auf mögliche Angriffsszenarien.",
            "Sie sollten die von ihnen entwickelte Client-Server-Anwendung zu einem einfachen IoT-System erweitern können, das auch von außerhalb des lokalen Netzwerks zugänglich ist, und dabei Sicherheitsmaßnahmen zum Schutz vor Angriffen ergreifen.",
            "Schließlich sollen die Schüler*innen in der Lage sein, die Chancen und Risiken des IoT aus individueller und gesellschaftlicher Sicht zu diskutieren und ihren eigenen Umgang mit IoT-Systemen zu reflektieren.",
            "Zu den behandelten Themen gehören: das Internet der Dinge (einschließlich Sensoren, Aktoren, Clients und Server) und verschiedene Sicherheitsmaßnahmen (organisatorische wie Richtlinien und Passwortregeln, technische wie Antivirensoftware und Firewalls, und physische wie Zugangskontrollen)."
        ] },
    { id: 2, value: 'Künstliche Intelligenz', summary: '', units: 34, content: [
            "Die Schüler*innen erstellen eine Wissensbasis für ein abgeschlossenes reales Welt System, indem Fakten und Regeln angegeben werden",
            "Es wird erwartet, dass Anfragen mittels einer Software an die erstellte Wissensbasis erstellt werden, um verschiedene Fragestellungen zu lösen",
            "Die Schüler*innen sollten in der Lage sein, die Strategie der automatisierten Herleitung von Aussagen durch eine Inferenzmaschine zu erläutern und zu erforschen",
            "Die Schüler*innen lernen die Struktur eines künstlichen neuronalen Netzes zu erklären und führen Forward Propagation auf gegebenen Eingabewerten durch",
            "Mit einer geeigneten Software analysieren die Schüler*innen den Einfluss von Trainingsdaten und Hyperparametern auf das Lernen von künstlichen neuronalen Netzen",
            "Die Schüler*innen lernen den k-Means-Algorithmus zur Implementierung des unüberwachten Lernens",
            "Analyse der Ergebnisse des k-Means-Algorithmus für verschiedene Eingabewerte",
            "Erläuterung der Grundprinzipien des überwachten, unüberwachten und verstärkenden Lernens",
            "Anwendung von maschinellem Lernen auf ein Problem mit angemessener Softwareunterstützung zur Datenvorbereitung und Parameteranpassung",
            "Bewertung und Diskussion der aktuellen Einsatzmöglichkeiten des maschinellen Lernens unter Berücksichtigung von Chancen, Risiken, individueller und gesellschaftlicher Verantwortung",
            "Unterrichtsinhalte umfassen wissensbasierte Systeme, neuronale Netzwerke, Clustering, k-Means-Algorithmus, maschinelles Lernen und ethische Fragen im Bereich der künstlichen Intelligenz"
        ] },
    { id: 3, value: 'Formale Sprachen und Automaten', summary: '', units: 24, content: [
            "Die Schüler*innen untersuchen formale Sprachen wie Autokennzeichen und E-Mail-Adressen und formulieren Regeln zur Bildung der Worte dieser Sprachen.",
            "Sie definieren Grammatiken zur Erzeugung formaler Sprachen, wobei sie zur Notation der Produktionsregeln die Erweiterte Backus-Naur-Form (EBNF) und Syntaxdiagramme verwenden.",
            "Sie entwerfen endliche Automaten zum Erkennen regulärer Sprachen.",
            "Sie implementieren deterministische endliche Automaten zur automatisierten Überprüfung der Zugehörigkeit von Wörtern zu einer regulären Sprache.",
            "Sie erläutern das Konzept des Nichtdeterminismus bei Automaten anhand geeigneter Beispiele.",
            "Es wird an Beispielen wie tief geschachtelten Klammerausdrücken erläutert, dass es Sprachen gibt, die nicht regulär sind, was darauf hinweist, dass das Modell des endlichen Automaten für die automatisierte Verarbeitung von nicht regulären Sprachen nicht ausreicht.",
            "Sie beschreiben das Modell der Turingmaschine und erläutern ihre Funktionsweise bei der Erkennung von Sprachen anhand von Beispielen.",
            "Sie entwerfen Turingmaschinen für einfache Beispiele Turing-erkennbarer Sprachen.",
            "Die Inhalte zu den Kompetenzen umfassen formalen Sprache als Menge von Wörtern über einem Alphabet, Grammatik, Notation formaler Sprachen, Ableitung eines Wortes einer formalen Sprache, endlicher Automat, Automat und Turingmaschine."
        ] },
    { id: 4, value: 'Algorithmen, Komplexität und Berechenbarkeit', summary: '', units: 32, content: [
            "Die Schüler*innen sollten den Begriff Turing-Berechenbarkeit erklären können und für einfache Funktionen eine passende Turingmaschine angeben können.",
            "Die Schüler*innen sollten in der Lage sein, den Laufzeitaufwand von Algorithmen durch Zeitmessungen und Zählverfahren zu bewerten.",
            "Die Schüler*innen sollten das asymptotische Laufzeitverhalten von Algorithmen unter Verwendung der Landau-Notation beschreiben können.",
            "Die Schüler*innen sollten begründen können, dass ein hoher Laufzeitaufwand sicherheitsrelevant sein kann.",
            "Die Schüler*innen sollten Algorithmen zur Lösung von Problemen entwerfen und implementieren können. Dazu sollten sie geeignete Strategien anwenden, passende Datenstrukturen wählen und ggf. das Problem auf ein anderes zurückführen können.",
            "Die Schüler*innen sollten Algorithmen zur Lösung eines Problems vergleichen und beurteilen können, insbesondere hinsichtlich des Laufzeitverhaltens und der Komplexität des Problems.",
            "Die Schüler*innen sollten die Definition der Komplexitätsklassen P und NP sowie die Bedeutung der Fragestellung P≟NP erklären können.",
            "Die Schüler*innen sollten das Halteproblem als Beispiel für nicht berechenbare Probleme erklären können.",
            "Zu den zu erlernenden Inhalten gehören: Berechenbarkeit, Laufzeitaufwand von Algorithmen, verschiedene Algorithmen, die Landausche O-Notation, verschiedene Lösungsstrategien, verschiedene Probleme, die Komplexität von Problemen und Algorithmen, die Mengen P und NP und das Halteproblem."
        ] }
];
//# sourceMappingURL=support.js.map