import { LitElement, PropertyValues, html } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { styleMap } from 'lit-html/directives/style-map.js';
import {
  animate,
  AnimateController,
  fadeInSlow,
  fadeOut,
} from '@lit-labs/motion';
import { onFrames, info_ga, info_ea, info_spät, info_ea_13, info_spät_13, DataItem } from './support.js';
import { styles } from './styles.js';


@customElement('lp-inf')
export class LpInf extends LitElement {

  static override styles = styles;

  @property({ type: Number, attribute: true }) stufe = 12;
  @property({ type: Array }) data_ga = this.calculateStartEnd(info_ga, 2);
  @property({ type: Array }) data_ea = this.calculateStartEnd(info_ea, 1);
  @property({ type: Array }) data_spät = this.calculateStartEnd(info_spät, 3);

  @property({ type: Array }) data = [...this.data_ga, ...this.data_ea, ...this.data_spät];

  @state() detail!: DataItem & { 'uid': number };

  controller = new AnimateController(this, {
    defaultOptions: {
      keyframeOptions: {
        duration: 500,
        fill: 'both',
      },
      onFrames,
    },
  });

  override updated(changedProperties: PropertyValues) {
    if (changedProperties.has('stufe')) {
      this.setContent();
    }
  }

  setContent() {
    if (this.stufe == 13) {
      console.log("update");
      this.data_ga = this.calculateStartEnd(info_spät_13, 2);
      this.data_ea = this.calculateStartEnd(info_ea_13, 1);
      this.data_spät = this.calculateStartEnd(info_spät_13, 3);
      this.data = [...this.data_ea, ...this.data_ga, ...this.data_spät]
    }

  }

  calculateStartEnd(data: DataItem[], column: number) {
    let res = [];
    let start = 2;
    for (let d of data) {
      res.push({ ...d, 'start': start, 'end': start + d.units, 'column': column, 'uid': 100 * column + d.id });
      start += d.units;
    }
    return res;
  }



  override render() {
    console.log("render");
    return html`<div>
      <div class="A fit cards grid-container">

      ${this.detail ? null : html`<div class="element title" style="grid-row: 1; grid-column:2">
                                      <div class="card-header-title">Grundlegendes Anforderungsniveau</div>
                                    </div>
                                    <div class="element title" style="grid-row: 1; grid-column:1">
                                      <div class="card-header-title">Erhöhtes Anforderungsniveau</div>
                                    </div>
                                    <div class="element title" style="grid-row: 1; grid-column:3">
                                      <div class="card-header-title">Spätbeginnend</div>
                                    </div>`}
        ${repeat(
      this.detail ? [] : this.data,
      (i) => i,
      (i, _) => {
        const style1 = { "grid-row": `${i.start} /${i.end}`, "grid-column": `${i.column}` };
        return html`<div class="element"
              style= ${styleMap(style1)}
              @click=${(e: Event) => this.clickHandler(e, i)}
              ${animate({
          out: fadeOut,
          id: `${i.uid}:card`,
          inId: `${i.uid}:detail`,
        })}
            >
              <div
                class="card-background fit"
                ${animate({
          in: fadeInSlow,
          skipInitial: true,
        })}
              ></div>
              
              
              <div class="card-header hero-text">
                <div
                  ${animate({
          id: `${i.uid}:card-header`,
          inId: `${i.uid}:detail-header`,
          skipInitial: true,
        })}
                >
                  <div class="card-header-title">${i.value}</div>
                  <div>${i.summary}</div>
                </div>
              </div>
            </div>`;
      }
    )}
      </div>
      ${this.detail
        ? html`<div
            class="detail fit"
            @click=${this.clickHandler}
            ${animate({
          id: `${this.detail.uid}:detail`,
          inId: `${this.detail.uid}:card`,
        })}
          >
            <div class="detail-header">
              <div
                class="icon detail-header-icon"
                ${animate({
          id: `${this.detail.uid}:detail-icon`,
          inId: `${this.detail.uid}:card-icon`,
        })}
              >
              
              </div>
              <div
                class="detail-header-text hero-text"
                ${animate({
          id: `${this.detail.uid}:detail-header`,
          inId: `${this.detail.uid}:card-header`,
        })}
              >
                <div class="detail-header-title">${this.detail.value}</div>
                <div>${this.detail.summary}</div>
              </div>
            </div>
            <div
              class="detail-content divider-top"
              ${animate({
          in: fadeInSlow,
        })}
            >
            <ul>
              ${repeat(this.detail.content,
          ((i) => i),
          (i, _) => {
            return html`<li>${i}</li>`;
          }

        )}
            </ul>    
            </div>
          </div>`
        : ''}
    </div>`;
  }

  clickHandler(_: Event, item: DataItem & { 'uid': number }) {
    if (this.controller.isAnimating) {
      this.controller.togglePlay();
    } else {
      this.detail = item;
    }
  }
}
