import {css} from 'lit';

export const styles = [
  css`
    :host {
      color: #040424;
      height: 100%;
      overflow: hidden;
      justify-content: center;
      --card-color: #546e7a;
      --card-text-color: white;
      --detail-color: #819ca9;
      --detail-text-color: white;
      --accent-color: #29434e;
      --divider: 2px solid var(--accent-color);
  
    }

    * {
      box-sizing: border-box;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    .grid-container {
      display: grid;
      grid-template-rows: 40px repeat(140, 1fr);
      grid-template-columns: repeat(3, 1fr);
      background-color:  #FFFFFF;
      height:100vh;
      grid-gap:4px;
    }
  
  .grid-container > div {
      background-color: #F87;
      border-radius: 4px;
      box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
      padding: 5px;
      will-change: transform;
      position: relative;
  }
  
  .header {
     grid-column: 1 / span 3;
      grid-row: 0;
  }
  
    .A {
      grid-column: 1 / span 1;
      grid-row: 1;
  
    }
    
    .fit {
      position: absolute;
      inset: 0;
    }

    .icon {
      font-family: 'Material Icons';
      font-style: normal;
      color: var(--accent-color);
    }

  
    .container {
      width: 800px;
      position: relative;
    }

    .cards {
      padding: 0;
      margin: 0;
    }

    .card-background {
      will-change: opacity;
     
    }

    .card-icon {
      will-change: transform;
      font-size: 8em;
      text-align: center;
      margin: 0px 0;
    }

    .card-content {
      align-items: center;
      justify-content: center;
    }

    .card-header {
      margin: auto;
    }

    .card-header-title {
      text-align: center;
      font-weight: 500;
      margin: auto;
    }

    .element {
      position: relative;
      will-change: transform;
      color: var(--detail-text-color);
      padding: 0px;
      overflow: hidden;
      background: var(--detail-color);
      display: flex;
      justify-content: center;
    }

    .title {
      background-color: #BD320F !important;
    }

    .detail-header-title {

      font-weight: 800;
    }

    .hero-text {
      will-change: transform;
      display: inline-block;
      width: 100%;
    }

    .detail-header-text {
      margin: 8px;

    }

    .detail-header-icon {
      will-change: transform;
      font-size: 3em;
      min-width: 48px;
    }

    .detail-content {
      padding: 16px;
      font-size: 1.1em;
      line-height: 200%;
      max-width: 1000px;
    }
  `,
];
