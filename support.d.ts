import { Animate } from '@lit-labs/motion';
export declare const springy: number[];
export declare const onFrames: (animate: Animate) => Keyframe[] | undefined;
export declare const info_ga: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export declare const info_spät: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export declare const info_ea: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export declare const info_ga_13: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export declare const info_spät_13: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export declare const info_ea_13: {
    id: number;
    value: string;
    summary: string;
    units: number;
    content: string[];
}[];
export type DataItem = typeof info_ga[number];
//# sourceMappingURL=support.d.ts.map